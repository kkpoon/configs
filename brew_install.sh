#!/bin/sh

# shell
brew install fish

# git
brew install git hub

# better terminal
brew install exa bat

# tools
brew install nvim fzf
